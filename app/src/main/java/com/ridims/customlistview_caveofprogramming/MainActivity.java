package com.ridims.customlistview_caveofprogramming;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static int[] programImages =
            {android.R.drawable.ic_input_add,
                    android.R.drawable.ic_delete,
                    android.R.drawable.ic_btn_speak_now,
                    android.R.drawable.ic_menu_close_clear_cancel};
    public static String[] programNameList = {"C", "Java", "C++", "PHP"};
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = (ListView) findViewById(R.id.listview);
        lv.setAdapter(new Adapter_CustomAdapter(this, programNameList, programImages));
    }
}
